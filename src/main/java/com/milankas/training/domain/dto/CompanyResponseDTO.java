package com.milankas.training.domain.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CompanyResponseDTO {

    private String id;
    private String name;
    private AddressResponseDTO address;

}
