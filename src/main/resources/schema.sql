CREATE TABLE IF NOT EXISTS companies
(
    id      uuid NOT NULL,
    name    character varying(50) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT companies_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS addresses
(
    id              UUID NOT NULL,
    address_line_1  CHARACTER VARYING(100) NOT NULL,
    address_line_2  CHARACTER VARYING(100) ,
    state           CHARACTER VARYING(20)  NOT NULL,
    city            CHARACTER VARYING(20)  NOT NULL,
    zip_code        CHARACTER VARYING(10)  NOT NULL,
    country_code    CHARACTER VARYING(10)  NOT NULL,
    company_id      UUID NOT NULL,
    CONSTRAINT      addresses_pkey PRIMARY KEY (id),
    CONSTRAINT fk_addresses_companies FOREIGN KEY (company_id)
    REFERENCES companies (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE CASCADE
    NOT VALID
);