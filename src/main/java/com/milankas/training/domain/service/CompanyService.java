package com.milankas.training.domain.service;

import com.milankas.training.domain.dto.AddressRequestDTO;
import com.milankas.training.domain.dto.CompanyRequestDTO;
import com.milankas.training.domain.dto.CompanyResponseDTO;
import com.milankas.training.domain.dto.CompanyUpdateDTO;
import com.milankas.training.persistence.crud.AddressCrudRepository;
import com.milankas.training.persistence.crud.CompanyCrudRepository;
import com.milankas.training.persistence.entity.Company;
import com.milankas.training.persistence.mapper.AddressMapper;
import com.milankas.training.persistence.mapper.CompanyMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class CompanyService {

    @Autowired
    private CompanyCrudRepository companyRepository;

    @Autowired
    private AddressCrudRepository addressRepository;

    @Autowired
    private CompanyMapper companyMapper;

    @Autowired
    private AddressMapper addressMapper;

    public List<CompanyResponseDTO> getAll() {
        return companyMapper.toCompanyResponseDTOList(companyRepository.findAll());
    }

    public Optional<CompanyResponseDTO> getCompany(String companyId) {
        return companyRepository.findById(UUID.fromString(companyId))
                .map(company -> companyMapper.toCompanyResponseDTO(company));
    }

    @Transactional
    public CompanyResponseDTO create(CompanyRequestDTO companyRequestDTO) {
        Company company = companyRepository.save(companyMapper.toCompany(companyRequestDTO));

        AddressRequestDTO addressRequestDTO = companyRequestDTO.getAddress();
        addressRequestDTO.setCompanyId(company.getId().toString());
        company.setAddress(addressRepository.save(addressMapper.toAddress(addressRequestDTO)));

        return companyMapper.toCompanyResponseDTO(company);
    }

    public boolean delete(String companyId) {
        return getCompany(companyId).map(companyResponseDTO -> {
            companyRepository.deleteById(UUID.fromString(companyId));
            return true;
        }).orElse(false);
    }

    public Optional<CompanyResponseDTO> update(CompanyUpdateDTO companyUpdateDTO, String companyId) {
        return companyRepository.findById(UUID.fromString(companyId))
                .map(company -> {
                    Company companyUpdate = companyMapper.updatePatchCompany(companyUpdateDTO, company);
                    return companyMapper.toCompanyResponseDTO(
                            companyRepository.save(companyUpdate));
                });
    }
}
