package com.milankas.training.domain.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.UUID;

@Getter
@Setter
public class AddressUpdateDTO {

    @JsonIgnore
    private UUID id;
    @Size(min = 3, max = 100)
    private String addressLine1;
    @Size(min = 3, max = 100)
    private String addressLine2;
    @Size(min = 3, max = 100)
    private String state;
    @Size(min = 3, max = 20)
    private String city;
    @Size(min = 3, max = 10)
    private String zipCode;
    @Pattern(regexp = "[A-Z]{2}", message = "Incorrect country code")
    private String countryCode;
    @JsonIgnore
    private UUID companyId;

}
