DELETE
FROM addresses;
DELETE
FROM companies;

INSERT INTO companies(id, name)
VALUES ('d8827313-8e7a-473a-8b27-40d8c71af632', 'Donec Corp.'),
       ('33f953f4-c913-428e-8329-6c8e29463467', 'Spring Corp.');

INSERT INTO addresses(id, address_line_1, address_line_2, state, city, zip_code, country_code, company_id)
VALUES ('f6a3d0fd-1a07-4214-b730-1ed9045e864a', '653-7632 Vulputate Av.', 'Ap #887-6389 Facilisi. St.', 'Colorado',
        'Aurora', '49275', 'US', 'd8827313-8e7a-473a-8b27-40d8c71af632'),
       ('e5d86aa3-e5db-4cc9-b528-70791ff84d2b', '653-7632 Center Av.', 'Ap #987-6389 Facilisi. St.', 'Dallas',
        'Aurora', '49275', 'DA', '33f953f4-c913-428e-8329-6c8e29463467');