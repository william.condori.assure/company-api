package com.milankas.training.domain.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.Size;
import java.util.UUID;

@Getter
@Setter
public class CompanyUpdateDTO {

    @JsonIgnore
    private UUID id;

    @Size(min = 3, max = 50)
    private String name;
    @Valid
    private AddressUpdateDTO address;

}
