package com.milankas.training.domain.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Getter
@Setter
public class AddressRequestDTO {

    @NotNull
    @Size(min = 3, max = 100)
    private String addressLine1;
    @Size(min = 3, max = 100)
    private String addressLine2;
    @NotNull
    @Size(min = 3, max = 100)
    private String state;
    @NotNull
    @Size(min = 3, max = 20)
    private String city;
    @NotNull
    @Size(min = 3, max = 10)
    private String zipCode;
    @NotNull
    @Pattern(regexp = "[A-Z]{2}", message = "Incorrect country code")
    private String countryCode;
    @JsonIgnore
    private String companyId;

}
