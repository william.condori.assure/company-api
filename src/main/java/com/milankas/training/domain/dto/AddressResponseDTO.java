package com.milankas.training.domain.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class AddressResponseDTO {

    private String addressLine1;
    private String addressLine2;
    private String state;
    private String city;
    private String zipCode;
    private String countryCode;

}
