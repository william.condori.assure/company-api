package com.milankas.training.persistence.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "companies")
public class Company {

    @Id
    @GeneratedValue
    private UUID id;
    private String name;
    @OneToOne(mappedBy = "company", cascade = CascadeType.ALL)
    private Address address;

}
