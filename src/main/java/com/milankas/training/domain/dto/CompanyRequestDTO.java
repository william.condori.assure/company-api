package com.milankas.training.domain.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
public class CompanyRequestDTO {

    @NotNull
    @Size(min = 3, max = 50)
    private String name;
    @Valid
    @NotNull
    private AddressRequestDTO address;

}
