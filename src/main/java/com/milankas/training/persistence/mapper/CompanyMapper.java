package com.milankas.training.persistence.mapper;

import com.milankas.training.domain.dto.CompanyRequestDTO;
import com.milankas.training.domain.dto.CompanyResponseDTO;
import com.milankas.training.domain.dto.CompanyUpdateDTO;
import com.milankas.training.persistence.entity.Company;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", uses = {HelperMapper.class, AddressMapper.class})
public interface CompanyMapper {

    @Mappings({
            @Mapping(target = "id", qualifiedByName = "UUIDtoString"),
    })
    CompanyResponseDTO toCompanyResponseDTO(Company company);

    List<CompanyResponseDTO> toCompanyResponseDTOList(List<Company> companies);

    @Mappings({
            @Mapping(target = "address", ignore = true)
    })
    Company toCompany(CompanyRequestDTO companyRequestDTO);

    Company toCompany(CompanyUpdateDTO companyUpdateDTO);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    @Mapping(target = "address.company", ignore = true)
    Company updatePatchCompany(CompanyUpdateDTO companyUpdateDTO, @MappingTarget Company company);

}
