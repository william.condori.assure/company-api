package com.milankas.training.persistence.crud;

import com.milankas.training.persistence.entity.Address;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface AddressCrudRepository extends JpaRepository<Address, UUID> {
}
