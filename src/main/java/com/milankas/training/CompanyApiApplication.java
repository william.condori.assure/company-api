package com.milankas.training;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class CompanyApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(CompanyApiApplication.class, args);
    }

}
