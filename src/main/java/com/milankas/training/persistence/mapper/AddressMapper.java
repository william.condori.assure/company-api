package com.milankas.training.persistence.mapper;

import com.milankas.training.domain.dto.AddressRequestDTO;
import com.milankas.training.domain.dto.AddressResponseDTO;
import com.milankas.training.domain.dto.AddressUpdateDTO;
import com.milankas.training.persistence.entity.Address;
import org.mapstruct.*;

@Mapper(componentModel = "spring", uses = {HelperMapper.class})
public interface AddressMapper {

    AddressResponseDTO toAddressResponseDTO(Address address);

    @Mapping(target = "companyId", qualifiedByName = "stringToUUID")
    Address toAddress(AddressRequestDTO addressRequestDTO);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    @Mapping(target = "company", ignore = true)
    Address updatePatchAddress(AddressUpdateDTO addressUpdateDTO, @MappingTarget Address address);
}
