package com.milankas.training.persistence.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "addresses")
public class Address {

    @Id
    @GeneratedValue
    private UUID id;
    @Column(name = "address_line_1")
    private String addressLine1;
    @Column(name = "address_line_2")
    private String addressLine2;
    private String state;
    private String city;
    @Column(name = "zip_code")
    private String zipCode;
    @Column(name = "country_code")
    private String countryCode;
    @Column(name = "company_id")
    private UUID companyId;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "company_id", referencedColumnName = "id", updatable = false, insertable = false)
    private Company company;

}
