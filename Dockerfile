FROM openjdk:8-jdk-alpine
VOLUME /tmp
ADD target/company-api-0.1.0.jar application.jar
ENTRYPOINT ["java", "-jar", "application.jar"]