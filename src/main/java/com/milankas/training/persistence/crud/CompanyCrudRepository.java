package com.milankas.training.persistence.crud;

import com.milankas.training.persistence.entity.Company;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface CompanyCrudRepository extends JpaRepository<Company, UUID> {
}
