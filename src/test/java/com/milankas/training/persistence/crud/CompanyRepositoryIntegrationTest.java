package com.milankas.training.persistence.crud;

import com.milankas.training.persistence.entity.Company;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.UUID;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class CompanyRepositoryIntegrationTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private CompanyCrudRepository companyCrudRepository;

    @Test
    void whenFindById_thenReturnCompany() {
        Company company = new Company();
        company.setId(UUID.randomUUID());
        company.setName("AssureSoft");
        entityManager.persist(company);
        entityManager.flush();

        Company companyFound = companyCrudRepository.findById(company.getId()).get();

        assertThat(companyFound.getName()).isEqualTo(company.getName());
    }
}