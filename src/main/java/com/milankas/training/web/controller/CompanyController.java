package com.milankas.training.web.controller;

import com.milankas.training.domain.dto.CompanyRequestDTO;
import com.milankas.training.domain.dto.CompanyResponseDTO;
import com.milankas.training.domain.dto.CompanyUpdateDTO;
import com.milankas.training.domain.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;

@Validated
@RestController
@RequestMapping("/companies")
public class CompanyController {

    @Autowired
    private CompanyService companyService;

    @GetMapping("/")
    public ResponseEntity<List<CompanyResponseDTO>> getAll() {
        return ResponseEntity.ok(companyService.getAll());
    }


    @GetMapping("/{companyId}")
    public ResponseEntity<CompanyResponseDTO> getCompany(@PathVariable("companyId") String companyId) {
        return companyService.getCompany(companyId)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @GetMapping( "/healthcheck")
    public ResponseEntity healthcheck(){
        HashMap<String, String> map = new HashMap<>();
        map.put("status", "Up");
        return ResponseEntity.ok(map);
    }

    @PostMapping("/")
    public ResponseEntity<CompanyResponseDTO> create(@Valid @RequestBody CompanyRequestDTO companyRequestDTO) {
        try {
            return ResponseEntity.status(HttpStatus.CREATED).body(companyService.create(companyRequestDTO));
        } catch (ConstraintViolationException | DataIntegrityViolationException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DeleteMapping("/{companyId}")
    public ResponseEntity delete(@PathVariable("companyId") String companyId) {
        if (companyService.delete(companyId)) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.notFound().build();
    }

    @PutMapping("/{companyId}")
    public ResponseEntity<CompanyResponseDTO> update(@Valid @RequestBody CompanyUpdateDTO companyUpdateDTO, @PathVariable("companyId") String companyId) {
        return companyService.update(companyUpdateDTO, companyId)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }
}
